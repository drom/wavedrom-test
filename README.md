# wavedrom-test

:construction: Work In Progress :construction:

Test of `wavedrom` Browser extension on GitLab

## Install

* [x] [Chrome extension](https://chrome.google.com/webstore/detail/wavedrom/ckplfgniaofnlpelgjkmbeeilfbbnboi)
* [x] [Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/wavedrom)

## Features

* replace `wavedrom` marked text sections with diagrams
  - [ ] [markdown](https://gitlab.com/drom/wavedrom-test/-/blob/master/test.md)
  - [ ] [adoc](https://gitlab.com/drom/wavedrom-test/-/blob/master/test.adoc)
  - [ ] [issues](https://gitlab.com/drom/wavedrom-test/-/issues/1)
  
:construction: Work In Progress :construction:
